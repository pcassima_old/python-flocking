"""
Still have to write the docstring.
"""

# TODO: Add perception angle to boids, currently they also "look" behind them

############################################# Imports ##############################################

from __future__ import annotations

from random import randint
from random import uniform

import pygame

from vector import Vector

######################################### Global variables #########################################

__author__ = 'P. Cassiman'
__version__ = '0.2.0'

############################################# Classes ##############################################


class Boid(object):
    """
    Class to represent boids for the flocking algorithm.
    """

    perception_radius = 50

    separation_factor = 0.25
    cohesion_factor = 2
    align_factor = 1

    def __init__(self):
        # self.position = Vector(WIDTH / 2, HEIGHT / 2)
        self.position = Vector(randint(0, WIDTH), randint(0, HEIGHT))
        self.velocity = Vector(uniform(-32, 32), uniform(-32, 32))
        self.acceleration = Vector()

        self.max_force = .5
        self.max_speed = 8

        self.update()

    def edges(self):
        """
        Method to allow the boids to wrap around the edges of the screen.
        """

        if self.position.x_coord > WIDTH:
            self.position.x_coord = 0
        elif self.position.x_coord < 0:
            self.position.x_coord = WIDTH

        if self.position.y_coord > HEIGHT:
            self.position.y_coord = 0
        elif self.position.y_coord < 0:
            self.position.y_coord = HEIGHT

        return

    def flock(self, boids):
        """
        Method to implement the three behaviors associated with the flocking algorithm.
        Alignment, cohesion and separation.

        Arguments:
            boids {list} -- The list of boids that compose the flock.
        """

        counter = 0

        align_vector = Vector()
        cohesion_vector = Vector()
        separation_vector = Vector()

        for other in boids:
            if other != self:
                distance = self.position.distance_to(other.position)
                if distance <= self.perception_radius:
                    # TODO: Add check for perception angle here

                    # Calculate the vector from self to other.
                    # Calculate the angle between the direction vector and the current heading.
                    # If the angle falls outside a certain angle, ignore it (aka other is behind).

                    counter += 1

                    align_vector += other.velocity

                    cohesion_vector += other.position

                    difference = self.position - other.position
                    if distance > 0:
                        difference /= distance
                        separation_vector += difference
                    counter += 1

            if counter > 0:
                align_vector /= counter
                # align_vector.set_magnitude(self.max_speed)
                align_vector -= self.velocity
                align_vector.limit(self.max_force)

                cohesion_vector /= counter
                cohesion_vector -= self.position
                # cohesion_vector.set_magnitude(self.max_speed)
                cohesion_vector -= self.velocity
                cohesion_vector.limit(self.max_force)

                separation_vector /= counter * 2
                # separation_vector.set_magnitude(self.max_speed)
                separation_vector -= self.velocity
                separation_vector.limit(self.max_force)

        self.acceleration -= (separation_vector * self.separation_factor)
        self.acceleration -= (align_vector * self.align_factor)
        self.acceleration -= (cohesion_vector * self.cohesion_factor)
        self.acceleration -= Vector(uniform(-.25, .25), uniform(-.25, .25))
        return

    def update(self):
        """
        Method to update the parametres of a boid.
        """
        # Add the acceleration to the velocity
        self.velocity += self.acceleration
        # Limit the velocity to the maximum speed
        self.velocity.limit(self.max_speed)
        # Add the velocity to the position (move the boid)
        self.position += self.velocity
        # Set the acceleration back to zero, means the boid will move straight if there are no
        # boids close by.
        self.acceleration = Vector(0, 0)

        return

    def show(self, surface):
        """
        Method to draw the boids on the given surface. The target is a pygame window.

        Arguments:
            surface {pygame surface} -- The surface to draw the boid on.
        """

        # Boids are represented by simple circles.
        pygame.draw.circle(surface,
                           (128, 255, 255),
                           (int(self.position.x_coord),
                            int(self.position.y_coord)),
                           3, 0)

        # Un-comment this block to see the perception radius around each boid.

        # pygame.draw.circle(surface,
        #                    (255, 0, 0),
        #                    (int(self.position.x_coord),
        #                     int(self.position.y_coord)),
        #                    self.perception_radius, 1)

############################################ Functions #############################################

############################################### Main ###############################################


if __name__ == "__main__":

    pygame.init()

    WIDTH = 1200
    HEIGHT = 600

    WIN = pygame.display.set_mode((WIDTH, HEIGHT))

    pygame.display.set_caption("Python flocking")

    RUN = True

    FLOCK = []
    for i in range(20):
        FLOCK.append(Boid())

    while RUN:
        pygame.time.delay(int(1000/60))

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                RUN = False

        WIN.fill((0, 0, 32))

        OLD_FLOCK = FLOCK
        for boid in FLOCK:
            boid.flock(OLD_FLOCK)
            boid.update()
            boid.edges()
            boid.show(WIN)

        pygame.display.update()

    pygame.quit()
