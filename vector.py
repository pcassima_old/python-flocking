"""
Still have to write the docstring.
"""

############################################# Imports ##############################################

from __future__ import annotations
from typing import Union

import math

######################################### Global variables #########################################

__author__ = 'P. Cassiman'
__version__ = '1.0.0'

############################################# Classes ##############################################


class Vector(object):
    """
    Class to represent points. This class is then used to represent higher shapes, such as lines,
    planes, polygons etc.
    The vector class is based on this class.
    """

    def __init__(self, x: [int, float] = 0, y: [int, float] = 0):
        """
        Constructor for the Vector class. The coordinates are given in a cartesian way.

        Keyword Arguments:
            x {int, float} -- The x coordinate of the point. (default: {0})
            y {int, float} -- The y coordinate of the point. (default: {0})
        """

        # Save the coordinates.
        self.x_coord = x
        self.y_coord = y

        return

    def __str__(self) -> str:
        """
        Method called when the str() method is used on the object.
        """
        return "({}, {})".format(self.x_coord, self.y_coord)

    def __repr__(self) -> str:
        """
        Method to return an informative representation of the vector object.
        """

        # Return a string representation of the vector.
        return "Vector at ({}, {})".format(self.x_coord, self.y_coord)

    def __eq__(self, other: Vector) -> bool:
        """
        Method called when the '==' operator is used on the object.
        """

        if isinstance(other, Vector):
            # If the other object is also a vector, we can compare them.
            if self.x_coord == other.x_coord and self.y_coord == other.y_coord:
                # If both vectors have the same x and y coordinates, they are equals.
                return True

            else:
                # If they do not have the same coordinates, they are not equals.
                return False

        else:
            # If the other object is not a vector raise TypeError.
            raise TypeError(
                "Expected Vector object for comparison, got{}".format(type(other)))

    def __lt__(self, other: Vector) -> bool:
        """
        Method called when the '<' operator is used on the object.
        """

        if isinstance(other, Vector):
            magnitude1, _ = self.cartesian_to_polar(self.x_coord, self.y_coord)
            magnitude2, _ = other.cartesian_to_polar(
                other.x_coord, other.y_coord)

            if magnitude1 < magnitude2:
                return True

            else:
                return False

        else:
            # If the other object is not a vector raise TypeError.
            raise TypeError(
                "Expected Vector object for comparison, got{}".format(type(other)))

    def __le__(self, other: Vector) -> bool:
        """
        Method called when the '<=' operator is used on the object.
        """

        if isinstance(other, Vector):
            magnitude1, _ = self.cartesian_to_polar(self.x_coord, self.y_coord)
            magnitude2, _ = other.cartesian_to_polar(
                other.x_coord, other.y_coord)

            if magnitude1 <= magnitude2:
                return True

            else:
                return False

        else:
            # If the other object is not a vector raise TypeError.
            raise TypeError(
                "Expected Vector object for comparison, got{}".format(type(other)))

    def __gt__(self, other: Vector) -> bool:
        """
        Method called when the '>' operator is used on the object.
        """

        if isinstance(other, Vector):
            magnitude1, _ = self.cartesian_to_polar(self.x_coord, self.y_coord)
            magnitude2, _ = other.cartesian_to_polar(
                other.x_coord, other.y_coord)

            if magnitude1 > magnitude2:
                return True

            else:
                return False

        else:
            # If the other object is not a vector raise TypeError.
            raise TypeError(
                "Expected Vector object for comparison, got{}".format(type(other)))

    def __ge__(self, other: Vector) -> bool:
        """
        Method called when the '>=' operator is used on the object.
        """

        if isinstance(other, Vector):
            magnitude1, _ = self.cartesian_to_polar(self.x_coord, self.y_coord)
            magnitude2, _ = other.cartesian_to_polar(
                other.x_coord, other.y_coord)

            if magnitude1 >= magnitude2:
                return True

            else:
                return False

        else:
            # If the other object is not a vector raise TypeError.
            raise TypeError(
                "Expected Vector object for comparison, got{}".format(type(other)))

    def __pos__(self) -> Vector:
        """
        Method called when the "+" operator is used on the point object. As a way of getting the
        positive value of the object.

        Returns:
            Vector -- A vector object with the new coordinates.
        """

        # Return a new vector with the current coordinates.
        return Vector(self.x_coord, self.y_coord)

    def __neg__(self) -> Vector:
        """
        Method called when the "-" operator is used on the point object. As a way of getting the
        negative value of the object.

        Returns:
            Vector -- A vector object with the new coordinates.
        """

        # Return a vector with inverted coordinates.
        return Vector(-self.x_coord, -self.y_coord)

    def __invert__(self) -> Vector:
        """
        Method called when the "~" operator is used on the point object. As a way of getting the
        negative value of the object (negating the object).

        This function relies on the __neg__ method, as they are the same in this context.

        Returns:
            Vector -- A vector object with the new coordinates.
        """

        # Return an inverse of self (a vector).
        return -self

    def __add__(self, other: Vector) -> Vector:
        """
        Method called when the "+" operator is used on the point object.

        Returns:
            Vector -- A vector object with the new coordinates.
        """

        if isinstance(other, Vector):
            # If the other object is another vector, add the coordinates.
            x_res = self.x_coord + other.x_coord
            y_res = self.y_coord + other.y_coord

        elif isinstance(other, (int, float)):
            # If the other object is a scalar, add the value to both coordinates.
            x_res = self.x_coord + other
            y_res = self.y_coord + other

        else:
            # If the other object has a different type than what we expected, raise an error.
            raise TypeError(
                "Expected Vector, int or float, got{}".format(type(other)))

        # Return a vector object with the new coordinates.
        return Vector(x_res, y_res)

    def __sub__(self, other: Vector) -> Vector:
        """
        Method called when the "-" operator is used on the vector object.

        Returns:
            Vector -- A vector object with the new coordinates.
        """

        if isinstance(other, Vector):
            # If the other object is another vector, subtract the coordinates.
            x_res = self.x_coord - other.x_coord
            y_res = self.y_coord - other.y_coord

        elif isinstance(other, (int, float)):
            # If the other object is a scalar, subtract the value from the coordinates.
            x_res = self.x_coord - other
            y_res = self.y_coord - other

        else:
            # If the other object has a different type than what we expected, raise an error.
            raise TypeError(
                "Expected Vector, int or float, got{}".format(type(other)))

        # Return a vector object with the new coordinates.
        return Vector(x_res, y_res)

    def __mul__(self, other: Union[int, float]) -> Vector:
        """
        Method called when the "*" operator is used on the vector object.

        Returns:
            Vector -- A vector object with the new coordinates.
        """

        if isinstance(other, (int, float)):
            # If the other object is a scalar, multiply the coordinates by the value.
            x_res = self.x_coord * other
            y_res = self.y_coord * other

        else:
            # If the other object has a different type than what we expected, raise an error.
            raise TypeError(
                "Expected int or float, got{}".format(type(other)))

        # Return a vector object with the new coordinates.
        return Vector(x_res, y_res)

    def __truediv__(self, other: Union[int, float]) -> Vector:
        if isinstance(other, (int, float)):
            if other != 0:
                x_res = self.x_coord / other
                y_res = self.y_coord / other
            else:
                raise ValueError("Cannot divide by zero!")

            return Vector(x_res, y_res)
        else:
            raise TypeError("Vector can only be divided by a scalar.")

    def limit(self, value: Union[int, float] = 1) -> Vector:
        """
        Method to limit the magnitude of a vector to the given value.

        Arguments:
            value {int, float} -- The limit in magnitude for the vector.
        """

        magnitude, theta = self.cartesian_to_polar(self.x_coord, self.y_coord)

        magnitude = min(magnitude, value)

        x_temp, y_temp = self.polar_to_cartesian(magnitude, theta)

        self.x_coord = x_temp
        self.y_coord = y_temp

        return

    def distance_to(self, other: Vector) -> float:
        """
        Method to calculate the distance between two vectors.

        Arguments:
            other {Vector} -- The vector to calculate the distance to.

        Raises:
            TypeError -- If the other object is not a Vector.

        Returns:
            float -- The distance to the other Vector.
        """

        if isinstance(other, Vector):
            return math.sqrt(((self.x_coord - other.x_coord) ** 2) +
                             ((self.y_coord - other.y_coord) ** 2))

        else:
            # If the other object has a different type than what we expected, raise an error.
            raise TypeError("Expected Vector, got{}".format(type(other)))

    def angle_to(self, other: Vector) -> float:
        """
        Cos(t) = (dot-product(u,v)) / (||u|| * ||v||)

        where ||v|| = the length (magnitude) of v

        Arguments:
            other {Vector} -- [description]

        Raises:
            NotImplementedError: [description]
            an: [description]
            TypeError: [description]

        Returns:
            float -- [description]
        """
        if isinstance(other, Vector):
            cos_t = ((self.dot_product(other)) /
                     (self.get_magnitude() * other.get_magnitude()))
            return math.degrees(math.acos(cos_t))

        else:
            # If the other object has a different type than what we expected, raise an error.
            raise TypeError("Expected Vector, got{}".format(type(other)))

    def dot_product(self, other: Vector) -> float:
        """
        Implements the dot-product for vectors;
        r = x1*x2 + y1*y2

        Arguments:
            other {Vector} -- [description]

        Returns:
            float -- [description]
        """
        return self.x_coord*other.x_coord + self.y_coord*other.y_coord

    def invert(self) -> Vector:
        """
        Method to return the inverse of a vector.
        """

        return -self

    def rotate(self, angle: Union[int, float] = 0) -> None:
        """
        Method to rotate a vector around the Origin. The angle is given in degrees.

        Keyword Arguments:
            angle {int, float} -- Angle to rotate by. (default: {0})
        """

        # Convert the angle to radians.
        angle = math.radians(angle)

        # Temporarily save the current coordinates.
        x_temp = self.x_coord
        y_temp = self.y_coord

        # Calculate the new coordinates.
        self.x_coord = (x_temp * math.cos(angle)) - (y_temp * math.sin(angle))
        self.y_coord = (x_temp * math.sin(angle)) + (y_temp * math.cos(angle))

        return

    def translate(self, vector: Vector) -> None:
        """
        Method to translate a vector by a second vector.

        Arguments:
            vector {Vector} -- The translation vector.
        """

        self.x_coord += vector.x_coord
        self.y_coord += vector.y_coord

        return

    def get_magnitude(self) -> float:
        """
        method to return the magnitude of a vector.

        Returns:
            float -- The magnitude of a vector.
        """

        magnitude, _ = self.cartesian_to_polar(self.x_coord, self.y_coord)

        return magnitude

    def set_magnitude(self, magnitude: Union[int, float]) -> None:
        """
        Method to set the magnitude of a vector.
        """

        _, theta = self.cartesian_to_polar(self.x_coord, self.y_coord)

        x_temp, y_temp = self.polar_to_cartesian(magnitude, theta)

        self.x_coord = x_temp
        self.y_coord = y_temp

        return

    def get_direction(self) -> float:
        """
        Method to return the direction of a vector.

        Returns:
            float -- The direction of a vector in degrees.
        """

        _, direction = self.cartesian_to_polar(self.x_coord, self.y_coord)

        return math.degrees(direction)

    def get_tuple(self) -> tuple:
        """
        Method to return a tuple of the coordinates of the vector.

        Returns:
            tuple -- tuple representing the coordinates.
        """

        return (self.x_coord, self.y_coord)

    def normalize(self, magnitude: Union[int, float] = 1) -> Vector:
        """
        Method to return a normalized vector with magnitude one.
        """

        _, direction = self.cartesian_to_polar(self.x_coord, self.y_coord)

        x_val, y_val = self.polar_to_cartesian(magnitude, direction)

        return Vector(x_val, y_val)

    @staticmethod
    def cartesian_to_polar(x_val: [int, float] = 0, y_val: [int, float] = 0):
        """
        Method to convert cartesian coordinates to polar coordinates.
        As opposed to the other methods in this class, this function returns arguments in radians.

        Keyword Arguments:
            x {int, float} -- x-coordinate (default: {0})
            y {int, float} -- y-coordinate (default: {0})

        Returns:
            float -- The magnitude of a vector.
            float -- The direction of a vector in radians.
        """

        if x_val != 0 and y_val != 0:
            # The magnitude is calculated using the pythagorean theorem.
            magnitude = math.sqrt((x_val ** 2) + (y_val ** 2))
            # The angle is calculated using the arc tangent.
            theta = math.atan(y_val / x_val)

            if x_val < 0:
                # If x is smaller than zero add 180 degrees to the angle (in radians).
                theta += math.pi

            if x_val > 0 and y_val < 0:
                # The fourth quadrant requires to add 360 degrees to the angle (to avoid negative
                # angle values).
                theta += math.tau

        elif x_val == 0 and y_val != 0:
            # If x is zero, the vector is pointing either up or down.
            # The magnitude is then depending on the y value.
            magnitude = abs(y_val)
            if y_val > 0:
                # If y is positive the angle is 90 degrees.
                theta = math.pi / 2
            else:
                # If y is positive the angle is 270 degrees.
                theta = math.pi * 3 / 2

        elif x_val != 0 and y_val == 0:
            # If y is zero, the vector is pointing either left or right.
            # The magnitude is then depending on the x value.
            magnitude = abs(x_val)
            if x_val > 0:
                # If x is positive the angle is 0 degrees.
                theta = 0
            else:
                # If x is positive the angle is 180 degrees.
                theta = math.pi
        else:
            # If we get some edge case, we return both the magnitude and angle as 0.
            magnitude = 0
            theta = 0

        # Modulo the angle for redundancy.
        theta = theta % math.tau
        # Return both parametres.
        return magnitude, theta

    @staticmethod
    def polar_to_cartesian(magnitude: [int, float], theta: [int, float]):
        """
        Method to convert polar coordinates to cartesian coordinates.
        As opposed to the other methods in this class, this function requires arguments in radians.

        Keyword Arguments:
            magnitude {int, float} -- magnitude (default: {0})
            theta {int, float} -- angle (default: {0})

        Returns:
            float -- The x-coordinate of a vector.
            float -- The y-coordinate of a vector.
        """

        # Calculate the x value.
        x_val = magnitude * math.cos(theta)
        # Calculate the y value.
        y_val = magnitude * math.sin(theta)

        return x_val, y_val

############################################ Functions #############################################

############################################### Main ###############################################


if __name__ == "__main__":
    pass
